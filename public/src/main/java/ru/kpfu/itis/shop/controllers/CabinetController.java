package ru.kpfu.itis.shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.itis.shop.aspects.annotation.IncludeParameters;
import ru.kpfu.itis.shop.aspects.annotation.IncludeUrlParam;
import ru.kpfu.itis.shop.security.MyUserDetail;
import ru.kpfu.itis.shop.service.AddressService;
import ru.kpfu.itis.shop.service.OrdersService;

/**
 * Created by Adel on 06.03.2016.
 */
@Controller
@RequestMapping("/cabinet")
public class CabinetController extends BaseController {
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private AddressService addressService;

    @IncludeUrlParam
    @IncludeParameters
    @RequestMapping(method = RequestMethod.GET)
    public String renderCabinet() {
        return "cabinet/cabinet";
    }

    /*
     * Отмена заказа, если это возможно
     */
    @ResponseBody
    @RequestMapping(value = "/deleteOrder", method = RequestMethod.POST)
    public String deleteOrder(Long id) { //это id заказа, который надо удалить
        if (ordersService.orderDeletion(id)) return "ok"; //если заказ удален
        else return "no"; //если заказ не может быть удален
    }

    /*
     * Удаление адреса пользователя
     */
    @ResponseBody
    @RequestMapping(value = "/deleteAddress", method = RequestMethod.POST)
    public String deleteAddress(Long id) {
        MyUserDetail user = (MyUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (addressService.deleteAddress(id, user.getUserInfo().getId())) return "ok"; //если адрес удален
        else return "no"; //если адрес не может быть удален
    }
}
