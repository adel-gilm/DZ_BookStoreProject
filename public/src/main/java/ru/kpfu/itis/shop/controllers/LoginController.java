package ru.kpfu.itis.shop.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.shop.form.RegistrationFormBean;

/**
 * Created by Adel on 30.03.2016.
 */
@Controller
@RequestMapping
public class LoginController extends BaseController {
    public static final String FORM = "form";

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String renderLoginPage(Boolean error) {
        request.setAttribute("error", error);
        request.setAttribute(FORM, new RegistrationFormBean());
        return "login";
    }

}
