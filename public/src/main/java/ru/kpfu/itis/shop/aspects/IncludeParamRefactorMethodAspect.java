package ru.kpfu.itis.shop.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.shop.form.AddAddressFormBean;
import ru.kpfu.itis.shop.form.RefactorFormBean;
import ru.kpfu.itis.shop.security.MyUserDetail;
import ru.kpfu.itis.shop.service.UsersService;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by Adel on 31.03.2016.
 */
@Aspect
@Component
public class IncludeParamRefactorMethodAspect {
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private UsersService usersService;

    public static final String REFACTOR_INF = "refForm";
    public static final String ADD_ADDRESS_FORM = "addressForm";

    @Pointcut("@annotation(ru.kpfu.itis.shop.aspects.annotation.IncludeParamInRefactorMethods)")
    public void includeParamRefactorMethod() {
    }

    @Before("includeParamRefactorMethod()")
    public void includeParameters() {
        request.setAttribute(REFACTOR_INF, new RefactorFormBean());
        request.setAttribute(ADD_ADDRESS_FORM, new AddAddressFormBean());
        MyUserDetail user = (MyUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        request.setAttribute("name", usersService.getUserName(user.getUserInfo().getLogin()));
    }
}
