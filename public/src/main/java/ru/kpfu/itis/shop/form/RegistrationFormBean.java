package ru.kpfu.itis.shop.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by Adel on 16.03.2016.
 */
public class RegistrationFormBean {
    @NotEmpty(message = "Поле обязательно для заполнения")
    private String userName;

    @Pattern(regexp = "[\\D\\s]+, [\\D\\s]+, [\\D\\s]+, [\\d]{1,5} - [\\d\\D]{1,5}, \\d{6}",
            message = "Пример: Российская Федерация, Казань, Восстания, 80 - 1, 421001")
    private String address;

    @Pattern(regexp = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}",
            message = "Неверный формат")
    private String login;

    @Size(min = 3, max = 8, message = "Пароль должен быть от 3 до 8 символов")
    private String password;

    @Size(min = 3, max = 8, message = "Пароль должен быть от 3 до 8 символов")
    private String confirmPassword;

    public RegistrationFormBean() {
    }

    public RegistrationFormBean(String userName, String address, String login, String password, String confirmPassword) {
        this.userName = userName;
        this.address = address;
        this.login = login;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @Override
    public String toString() {
        return "RegistrationFormBean{" +
                "userName='" + userName + '\'' +
                ", address='" + address + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                '}';
    }
}
