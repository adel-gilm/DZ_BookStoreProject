package ru.kpfu.itis.shop.util;

import ru.kpfu.itis.shop.model.Goods;

import java.util.List;

/**
 * Created by Adel on 15.04.2016.
 */
public class CartList {
    private List<Goods> list;

    public CartList(List<Goods> list) {
        this.list = list;
    }

    public List<Goods> getList() {
        return list;
    }

    public void setList(List<Goods> list) {
        this.list = list;
    }

    public boolean contains(Goods good){
        return list.contains(good);
    }
}
