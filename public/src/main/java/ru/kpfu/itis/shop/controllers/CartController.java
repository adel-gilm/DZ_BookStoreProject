package ru.kpfu.itis.shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.shop.model.Cart;
import ru.kpfu.itis.shop.security.MyUserDetail;
import ru.kpfu.itis.shop.service.AddressService;
import ru.kpfu.itis.shop.service.CartService;
import ru.kpfu.itis.shop.service.GoodsService;
import ru.kpfu.itis.shop.service.OrdersService;
import ru.kpfu.itis.shop.util.Methods;

import java.io.IOException;
import java.util.List;

/**
 * Контроллер для работы с корзиной
 * Created by Adel on 06.03.2016.
 */
@Controller
@RequestMapping("/cart")
public class CartController extends BaseController {
    @Autowired
    private CartService cartService;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private GoodsService goodsService;

    /*
     * Отображает все добавленно в корзину. Достается либо из сессии, либо из бд
     */
    @RequestMapping
    public String renderCart(ModelMap model) {
        String bookList = (String) request.getSession().getAttribute("books");
        List<Cart> books;
        if (!SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            if (bookList != null && !bookList.isEmpty()) {
                cartService.addAllBooksFromSession(bookList, Methods.getUserId());
                request.getSession().removeAttribute("books");
            }
            MyUserDetail user = (MyUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            books = cartService.getAllGoodsFromCart(Methods.getUserId());
            request.setAttribute("bookList", books);
            request.setAttribute("addresses", addressService.getAddress(user.getUserInfo().getId()));
        } else {
            books = cartService.getAnonBooks(bookList);
            request.setAttribute("bookList", books);
        }
        if (books != null) {
            model.put("sum", Methods.getSum(books));
            model.put("amount", Methods.getAmount(books));
        }
        return "cart";
    }

    /*
     * Обработка действия над книгой, либо ее полное удаление, либо ее покупка
     */
    @RequestMapping(value = "/action/{id}", method = RequestMethod.POST)
    public String renderActionInCart(@PathVariable("id") Long bookId) {
        String delete = request.getParameter("delete");
        if (delete != null) {
            if (!SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser"))
                cartService.deleteBookInCart(Methods.getUserId(), bookId);
            else Methods.deleteBookInCart(request, bookId);
        }
        return "redirect:/cart";
    }

    /*
     * Увеличение кол-ва книг в корзине, или первое добавление книги в корзину
     */
    @ResponseBody
    @RequestMapping(value = "/increase/{id}", method = RequestMethod.POST)
    public String addBookInCart(Long id) throws IOException {
        if (!SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            cartService.increaseBook(Methods.getUserId(), id);
        } else {
            request.getSession().setAttribute("books", Methods.addBookInCartMethod(request, id));
        }
        return String.valueOf(goodsService.getBook(id).getPrice());
    }

    /*
     * Уменьшение кол-ва книги на одну, не работает, если книга осталась а корзине одна
     */
    @ResponseBody
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.POST)
    public String deleteOneBookInCart(Long id) throws IOException {
        if (!SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            cartService.removeBook(Methods.getUserId(), id);
        } else {
            request.getSession().setAttribute("books", Methods.deleteOneBookInCartMethod(request, id));

        }
        return String.valueOf(goodsService.getBook(id).getPrice());
    }

    /*
     * Оформление заказа для авторизованного пользователя и редирект на страницу регистрации для неавторизованного
     */
    @RequestMapping(value = "/order_processing", method = RequestMethod.POST)
    public String addOrderPOST(@RequestParam String address) {
        if (!SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            ordersService.orderAddition(Methods.getUserId(), Long.parseLong(address));
            return "redirect:/cabinet";
        } else {
            return "redirect:/enter";
        }
    }


}
