package ru.kpfu.itis.shop.util;

import org.springframework.security.core.context.SecurityContextHolder;
import ru.kpfu.itis.shop.model.Cart;
import ru.kpfu.itis.shop.security.MyUserDetail;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Adel on 31.03.2016.
 */
public class Methods {
    /*
     * Удаление книиги из корзины для не авторизованного пользователя, то есть из сессии
     */
    public static void deleteBookInCart(HttpServletRequest request, Long bookId) {
        String bookList = "";
        //удаление книги из сессии
        if (request.getSession().getAttribute("books") != null) { //если список не пуст
            //поменять значение в сессии
            bookList = (String) request.getSession().getAttribute("books");
            String book[] = bookList.split(";");
            bookList = "";
            for (String aBook : book) {
                String amount[] = aBook.split("-");
                if (Long.parseLong(amount[0]) != bookId) {
                    bookList += amount[0] + "-" + amount[1] + ";";
                }
            }
        }
        request.getSession().setAttribute("books", bookList);
    }

    /*
     * Вспомогательный метод для увеличение кол-ва книг в корзине,
     * или первое добавление книги в корзину
     */
    public static String addBookInCartMethod(HttpServletRequest request, Long id) {
        //books в сессии, bookList в запросе
        String bookList = "";
        //добавление книги в сессию
        String sessionBooks = (String) request.getSession().getAttribute("books");
        if (sessionBooks == null || sessionBooks.equals("")) { //если книги не добавлялись
            bookList = id + "-1;";
        } else {
            boolean exist = false;
            //поменять значение в сессии
            bookList = (String) request.getSession().getAttribute("books");
            String book[] = bookList.split(";");
            bookList = "";
            for (String aBook : book) {
                String amount[] = aBook.split("-");
                if (Long.parseLong(amount[0]) == id) {
                    exist = true;
                    Long count = Long.parseLong(amount[1]) + 1;
                    amount[1] = count.toString();
                }
                bookList += amount[0] + "-" + amount[1] + ";";
            }
            if (!exist) {
                bookList += id + "-1;";
            }
        }
        return bookList;
//        JSONObject jo = new JSONObject();
//        jo.put("results", bookList);
//        response.getWriter().print(jo.toString());
    }

    /*
     * Вспомогательный метод для уменьшение кол-ва книги на одну,
     * не работает, если книга осталась а корзине одна
     */
    public static String deleteOneBookInCartMethod(HttpServletRequest request, Long id) {
        String bookList = "";
        //удаление книги из сессии
        if (request.getSession().getAttribute("books") != null) { //если список не пуст
            //поменять значение в сессии
            bookList = (String) request.getSession().getAttribute("books");
            String book[] = bookList.split(";");
            bookList = "";
            for (int i = 0; i < book.length; i++) {
                String amount[] = book[i].split("-");
                if (Long.parseLong(amount[0]) == id) {
                    if (Long.parseLong(amount[1]) > 1) {//если к корзине было несколько книг
                        Long count = Long.parseLong(amount[1]) - 1;
                        amount[1] = count.toString();
                    }
                }
                bookList += amount[0] + "-" + amount[1] + ";";
            }
        }
        return bookList;
//        JSONObject jo = new JSONObject();
//        jo.put("results", bookList);
//        response.getWriter().print(jo.toString());
    }

    /*
     * Метод для добавление  книги в корзину для неавторизованного пользователя
     */
    public static String addBookInCart(HttpServletRequest request, Long id) {
        //books в сессии, bookList в запросе
        String bookList = "";
        //добавление книги в сессию
        String sessionBooks = (String) request.getSession().getAttribute("books");
        if (sessionBooks == null || sessionBooks.equals("")) { //если книги не добавлялись
            bookList = id + "-1;";
            return bookList;
        } else {
            return addBookInCartMethod(request, id);
        }

    }

    /*
     * Получение id пользователя черерз spring security
     */
    public static Long getUserId() {
        MyUserDetail user = (MyUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUserInfo().getId();
    }

    /*
     * Генерация ключа
     */
    public static String getKey() {
        String key = "";
        String values = "qwertyuiopasdfghjlzxcvbnm123456789012345678901234567890";
        for (int i = 0; i < 40; i++) {
            int index = (int) (Math.random() * 54);
            key += values.charAt(index);
        }
        return key;
    }

    /*
     * Высчитывает общую сумму всех книг в корзине
     */
    public static Double getSum(List<Cart> books) {
        Double sum = (double)0;
        for (Cart book : books) {
            sum += book.getCount() * book.getGoods().getPrice();
        }
        return sum;
    }

    /*
     * Высчитывает общее кол-во товаров в корзине
     */
    public static Long getAmount(List<Cart> books) {
        Long amount = (long)0;
        for (Cart book : books) {
            amount += book.getCount();
        }
        return amount;
    }
}
