package ru.kpfu.itis.shop.controllers;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.shop.Mail;
import ru.kpfu.itis.shop.aspects.annotation.IncludeParamInRefactorMethods;
import ru.kpfu.itis.shop.form.AddAddressFormBean;
import ru.kpfu.itis.shop.form.RefactorFormBean;
import ru.kpfu.itis.shop.security.MyUserDetail;
import ru.kpfu.itis.shop.service.AddressService;
import ru.kpfu.itis.shop.service.UsersService;

import javax.validation.Valid;
import java.io.IOException;

/**
 * Контроллер для изменения данных пользователя
 * Created by Adel on 03.04.2016.
 */
@Controller()
@RequestMapping("cabinet/refactor")
public class RefactoringController extends BaseController {
    public static final String REFACTOR_INF = "refForm";
    public static final String ADD_ADDRESS_FORM = "addressForm";
    @Autowired
    private UsersService usersService;
    @Autowired
    private AddressService addressService;

    @IncludeParamInRefactorMethods
    @RequestMapping(method = RequestMethod.GET)
    public String showForm() {
        return "cabinet/data_refactoring";
    }

    /*
     * Изменение персональных данных
     */
    @IncludeParamInRefactorMethods
    @RequestMapping(value = "/inf", method = RequestMethod.POST)
    public String refactoring(@Valid @ModelAttribute(REFACTOR_INF) RefactorFormBean refactorFormBean,
                              BindingResult bindingResult) throws IOException {
        MyUserDetail user = (MyUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (bindingResult.hasErrors()) {
            return "cabinet/data_refactoring";
        }
        String name = refactorFormBean.getUserName();
        String login = refactorFormBean.getLogin();
        String password = refactorFormBean.getPassword();
        if (!name.isEmpty()) {
            usersService.setName(name, user.getUserInfo().getLogin());
        }
        if (!login.isEmpty()) {
            usersService.setLogin(login, user.getUserInfo().getLogin());
            new Mail().loginChangingText(user.getUserInfo().getName(), user.getUserInfo().getLogin(), login);
            user.getUserInfo().setLogin(login);
        }
        if (!password.isEmpty()) {
            String hash_pass = DigestUtils.md5Hex(password);
            usersService.setPassword(hash_pass, user.getUserInfo().getLogin());
            user.getUserInfo().setHash_pass(hash_pass);
            new Mail().passwordChangingText(user.getUserInfo().getName(), user.getUserInfo().getLogin(), password);
        }
        return "redirect:/cabinet";
    }

    /*
     * Добавление нового адреса
     */
    @IncludeParamInRefactorMethods
    @RequestMapping(value = "/add_address",method = RequestMethod.POST)
    public String addAddress(@Valid @ModelAttribute(ADD_ADDRESS_FORM) AddAddressFormBean addAddressFormBean,
                             BindingResult bindingResult) {
        MyUserDetail user = (MyUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (bindingResult.hasErrors()) {
            return "cabinet/data_refactoring";
        }
        addressService.addAddress(addAddressFormBean.getArea(), addAddressFormBean.getCity(),
                addAddressFormBean.getStreet(), addAddressFormBean.getHouse(), addAddressFormBean.getFlat(),
                addAddressFormBean.getIndex(), user.getUserInfo().getId());
        System.out.println(addAddressFormBean);
        return "redirect:/cabinet";
    }
}
