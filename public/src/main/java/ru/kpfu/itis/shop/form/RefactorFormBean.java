package ru.kpfu.itis.shop.form;

import javax.validation.constraints.Pattern;

/**
 * Created by Adel on 16.03.2016.
 */
public class RefactorFormBean {
    private String userName;

    @Pattern(regexp = "|([A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4})",
            message = "Неверный формат")
    private String login;

    @Pattern(regexp = "|(.{3,8})",message = "Пароль должен быть от 3 до 8 символов")
    private String password;

    @Pattern(regexp = "|(.{3,8})", message = "Пароль должен быть от 3 до 8 символов")
    private String confirmPassword;

    public RefactorFormBean() {
    }

    public RefactorFormBean(String userName, String login, String password, String confirmPassword) {
        this.userName = userName;
        this.login = login;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @Override
    public String toString() {
        return "RefactorFormBean{" +
                "userName='" + userName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                '}';
    }
}
