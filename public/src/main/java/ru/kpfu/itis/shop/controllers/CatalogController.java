package ru.kpfu.itis.shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.shop.model.Goods;
import ru.kpfu.itis.shop.service.CartService;
import ru.kpfu.itis.shop.service.CategoryService;
import ru.kpfu.itis.shop.service.GoodsService;
import ru.kpfu.itis.shop.util.CartList;
import ru.kpfu.itis.shop.util.Constants;
import ru.kpfu.itis.shop.util.Methods;
import ru.kpfu.itis.shop.util.SortingFilter;

import java.util.List;

/**
 * Created by Adel on 06.03.2016.
 */
@Controller
@RequestMapping("/catalog")
public class CatalogController extends BaseController {
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private CartService cartService;
    @Autowired
    private CategoryService categoryService;

    private Integer limit;
    private SortingFilter sortingFilter;
    private List<Goods> books;

    /*
     * Получение всех книг из базы
     */
    @RequestMapping
    public String renderCatalog(ModelMap model) {
        limit = 0;
        sortingFilter = new SortingFilter(0, "asc", (double) 0, (double) 0);
        model.put("sortingFilter", sortingFilter);
        books = goodsService.getAllGoods();
        request.setAttribute("books", books.subList(limit, limit + 6));
        request.setAttribute("categories", categoryService.getAllCategories());

        //фокусы с корзиной
        if (!SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            request.setAttribute("cart", new CartList(cartService.getListOfBooksFromCart(Methods.getUserId())));
        } else {
            String bookList = (String) request.getSession().getAttribute("books");
            request.setAttribute("cart", new CartList(cartService.getListOfAnonBooks(bookList)));
        }
        return "catalog/catalog";
    }

    /*
     * Получение информации об одной книге
     */
    @RequestMapping(value = "/book/{id}", method = RequestMethod.GET)
    public String renderBookView(@PathVariable("id") Long id) {
        request.setAttribute("book", goodsService.getBook(id));
        //фокусы с корзиной
        if (!SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            request.setAttribute("cart", new CartList(cartService.getListOfBooksFromCart(Methods.getUserId())));
        } else {
            String bookList = (String) request.getSession().getAttribute("books");
            request.setAttribute("cart", new CartList(cartService.getListOfAnonBooks(bookList)));
        }
        return "book_info";
    }

    /*
     * AJAX запрос на добавление книги в корзину
     */
    @ResponseBody
    @RequestMapping(value = "/addBook/{id}", method = RequestMethod.POST)
    public String renderAddBookInCart(Long id) {
        if (!SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            cartService.addBook(Methods.getUserId(), id);
        } else
            request.getSession().setAttribute("books", Methods.addBookInCart(request, id));
        return "ok";
    }

    /**
     * Показать еще товары
     */
    @RequestMapping(value = "/showMore", method = RequestMethod.POST)
    public String showMoreGoods() {
        limit += 6;
        if (books.size() > limit + 6) {
            request.setAttribute("books", books.subList(limit, limit + 6));
        } else {
            request.setAttribute("end", "end");
            request.setAttribute("books", books.subList(limit, books.size()));
        }
        return "catalog/ajaxGoods";
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    public String sort(@RequestParam(required = false, defaultValue = "0") String category,
                       @RequestParam(required = false, defaultValue = "asc") String sort,
                       @RequestParam(required = false, defaultValue = "0") String from,
                       @RequestParam(required = false, defaultValue = "0") String to) {
        limit = 0;
        Double min = (double) 0, max = Constants.maxValue;
        try {
            min = Double.parseDouble(from);
            if (!to.equals("0")) max = Double.parseDouble(to);
            sortingFilter.setFrom(Double.parseDouble(from));
            sortingFilter.setTo(Double.parseDouble(to));
            sortingFilter.setCategory(Integer.parseInt(category));
            sortingFilter.setSort(sort);
            request.setAttribute("sortingFilter", sortingFilter);
        } catch (NumberFormatException | NullPointerException ex) {
            request.setAttribute("sortingFilter", new SortingFilter(Integer.parseInt(category), sort, (double) 0, (double) 0));
        }
        books = goodsService.getSortingGoods(category, sort, min, max);
        if (books.size() > limit + 6) {
            request.setAttribute("books", books.subList(limit, limit + 6));
        } else {
            request.setAttribute("end", "end");
            request.setAttribute("books", books.subList(limit, books.size()));
        }
        request.setAttribute("categories", categoryService.getAllCategories());

        //фокусы с корзиной
        if (!SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser")) {
            request.setAttribute("cart", new CartList(cartService.getListOfBooksFromCart(Methods.getUserId())));
        } else {
            String bookList = (String) request.getSession().getAttribute("books");
            request.setAttribute("cart", new CartList(cartService.getListOfAnonBooks(bookList)));
        }
        return "catalog/catalog";
    }
}
