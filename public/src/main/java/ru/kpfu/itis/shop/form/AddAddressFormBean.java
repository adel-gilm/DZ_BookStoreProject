package ru.kpfu.itis.shop.form;

import javax.validation.constraints.Pattern;

/**
 * Created by Adel on 16.03.2016.
 */
public class AddAddressFormBean {
    @Pattern(regexp = "[\\D\\s]+", message = "Неверный формат")
    private String area;

    @Pattern(regexp = "[\\D\\s]+", message = "Неверный формат")
    private String city;

    @Pattern(regexp = "[\\D\\s]+", message = "Неверный формат")
    private String street;

    @Pattern(regexp = "\\d[\\d\\D]{1,4}", message = "Неверный формат")
    private String house;

    @Pattern(regexp = "|[\\d]{1,5}", message = "Неверный формат")
    private String flat;

    @Pattern(regexp = "\\d{6}", message = "Неверный формат")
    private String index;

    public AddAddressFormBean(String area, String city, String street, String house, String flat, String index) {
        this.area = area;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.index = index;
    }

    public AddAddressFormBean() {
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "AddAddressFormBean{" +
                "area='" + area + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", house='" + house + '\'' +
                ", flat='" + flat + '\'' +
                ", index='" + index + '\'' +
                '}';
    }
}
