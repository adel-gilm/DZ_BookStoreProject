package ru.kpfu.itis.shop.controllers;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.shop.Mail;
import ru.kpfu.itis.shop.form.RegistrationFormBean;
import ru.kpfu.itis.shop.service.AddressService;
import ru.kpfu.itis.shop.service.UsersService;
import ru.kpfu.itis.shop.util.Methods;

import javax.validation.Valid;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Adel on 10.03.2016.
 */
@Controller
@RequestMapping("/registration")
public class RegistrationController extends BaseController {
    public static final String FORM = "form";
    @Autowired
    private UsersService usersService;
    @Autowired
    private AddressService addressService;

    /*
     * Контроллер для регистрации пользователя
     */
    @RequestMapping(method = RequestMethod.GET)
    public String renderCart() {
        request.setAttribute(FORM, new RegistrationFormBean());
        return "registration";
    }

    /*
     * Обработка формы Регистрации
     */
    @RequestMapping(method = RequestMethod.POST)
    public String registrationMethod(@Valid @ModelAttribute(FORM) RegistrationFormBean registrationFormBean,
                                     BindingResult bindingResult)
            throws NoSuchAlgorithmException, IOException {
        if (bindingResult.hasErrors()) {
            return "login";
        }
        String key = Methods.getKey();
        String userName = registrationFormBean.getUserName();
        String login = registrationFormBean.getLogin();
        String hash_pass = DigestUtils.md5Hex(registrationFormBean.getPassword()); //хэширование

        //добавление нового пользователя в бд
        usersService.addUser(userName, login, hash_pass, key);

        System.out.println(registrationFormBean);
        new Mail().registrationText(userName, login, key);
        return "redirect:/cabinet";
    }

    /*
     * Проверка ключа отправденного пользователю и окончательная регистрация
     */
    @RequestMapping(value = "/verification/{login}/{key}", method = RequestMethod.GET)
    public String verificationMethod(@PathVariable("login") String login, @PathVariable("key") String key) {
        String needKey = usersService.getKey(login);
        if (needKey.equals(key)) {
            usersService.changeChecking(login);
            System.out.println();
            return "redirect:/cabinet";
        } else return "registration";
    }

    /*
     *  Проверка уникальности логина AJAX
     */
    @ResponseBody
    @RequestMapping(value = "/checkingLogin", method = RequestMethod.POST)
    public String checkingLogin(@RequestParam String login) {
        return usersService.checkLogin(login);
    }


}
