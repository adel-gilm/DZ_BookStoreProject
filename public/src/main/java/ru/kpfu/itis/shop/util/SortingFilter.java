package ru.kpfu.itis.shop.util;

/**
 * Created by Adel on 31.03.2016.
 */
public class SortingFilter {
    private Integer category;
    private String sort;
    private Double from, to;

    public SortingFilter() {    }

    public SortingFilter(Integer category, String sort, Double from, Double to) {
        this.category = category;
        this.sort = sort;
        this.from = from;
        this.to = to;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Double getFrom() {
        return from;
    }

    public void setFrom(Double from) {
        this.from = from;
    }

    public Double getTo() {
        return to;
    }

    public void setTo(Double to) {
        this.to = to;
    }
}
