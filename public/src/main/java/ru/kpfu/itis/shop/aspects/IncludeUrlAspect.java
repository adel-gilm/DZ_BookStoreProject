package ru.kpfu.itis.shop.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by Adel on 31.03.2016.
 */
@Aspect
@Component
public class IncludeUrlAspect {
    @Autowired
    private HttpServletRequest request;

    @Pointcut("@annotation(ru.kpfu.itis.shop.aspects.annotation.IncludeUrlParam)")
    public void includeUrlMethod() {
    }
    @Before("includeUrlMethod()")
    public void includeUrl() {
        request.setAttribute("url", request.getRequestURI());
    }
}
