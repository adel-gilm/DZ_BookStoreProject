package ru.kpfu.itis.shop.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.shop.security.MyUserDetail;
import ru.kpfu.itis.shop.service.AddressService;
import ru.kpfu.itis.shop.service.OrdersService;
import ru.kpfu.itis.shop.service.UsersService;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by Adel on 31.03.2016.
 */
@Aspect
@Component
public class IncludeParameterAspect {
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private OrdersService ordersService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private UsersService usersService;

    @Pointcut("@annotation(ru.kpfu.itis.shop.aspects.annotation.IncludeParameters)")
    public void includeParametersMethod() {
    }

    @Before("includeParametersMethod()")
    public void includeParameters() {
        MyUserDetail user = (MyUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        request.setAttribute("addresses", addressService.getAddress(user.getUserInfo().getId()));
        request.setAttribute("name", usersService.getUserName(user.getUserInfo().getLogin()));
        request.setAttribute("email", user.getUserInfo().getLogin());
        request.setAttribute("ordersList", ordersService.getOrders(user.getUserInfo().getId()));
    }
}
