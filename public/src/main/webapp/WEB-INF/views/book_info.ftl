<#-- @ftlvariable name="cart" type="ru.kpfu.itis.shop.util.CartList" -->
<#-- @ftlvariable name="book" type="ru.kpfu.itis.shop.model.Goods" -->
<#include "templates/main_template.ftl">
<@main_template title="Информация о книге" scripts=["catalog.js"]/>

<#macro body>
<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-9 padding-right">
                <div class="product-details"><!--product-details-->
                    <div class="col-sm-5">
                        <div class="view-product">
                            <img src=${(book.getImage())!}/>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="product-information"><!--/product-information-->
                            <h2>${(book.getName())!}</h2>

                            <p>${(book.getAuthor())!}</p>

                            <p>Жанр: ${(book.getCategory().getAlias())!}</p>
								<span>
									<span>${(book.getPrice())!} руб</span>
                                <#--Проверка есть ли в корзине содержимое, и сожержится ли этот товар в корзине-->
                                    <#if cart?? && cart.contains(book)>
                                        <a href="/cart" class="btn btn-default add-to-cart">
                                            <i class="fa fa-shopping-cart"></i>В корзине</a>
                                    <#else>
                                        <a data-id="${(book.getId())!}" href="/cart"
                                           class="btn btn-default add-to-cart js_addToCart">
                                            <i class="fa fa-shopping-cart"></i>Добавить</a>
                                    </#if>
								</span>
                        </div>
                        <!--/product-information-->
                    </div>
                </div>
                <!--/product-details-->


            </div>
        </div>
    </div>
</section>
</#macro>