<#assign form=JspTaglibs["http://www.springframework.org/tags/form"]>
<#include "../templates/main_template.ftl">
<@main_template title="Изменение данных" scripts=["refactor.js"]/>
<#macro body>
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form"><!--login form-->
                    <h2>Изменить личные данные</h2>
                    <@form.form commandName="refForm" action="/cabinet/refactor/inf" method="post">

                        <p><@form.input path="userName" value="${(name)!}" placeholder="Имя" onblur="validateName(this)" size="40" />
                        <@form.errors path="userName" cssStyle="color: red; font-size: 10px" /></p>

                        <p><@form.input path="login" onblur="validateEmail(this)" placeholder="Логин" oninput="checkLogin(this)" size="40"/>
                        <@form.errors path="login" cssStyle="color: red; font-size: 10px" /></p>

                        <p><@form.password path="password" id="pass" placeholder="Пароль" onblur="validatePassword(this)" size="40"/>
                        <@form.errors path="password" cssStyle="color: red; font-size: 10px" /></p>

                        <p><@form.password path="confirmPassword" id="pass2" placeholder="Повторный пароль" onblur="validateConfirmPassword(this)" size="40"/>
                        <@form.errors path="confirmPassword" cssStyle="color: red; font-size: 10px" /></p>

                        <button type="submit" class="btn btn-default">Изменить</button>
                    </@form.form>
                </div>
                <!--/login form-->
            </div>

            <div class="col-sm-1">
                <h2 class="or">Или</h2>
            </div>

            <div class="col-sm-4">
                <div class="signup-form"><!--sign up form-->
                    <h2>Добавить адрес</h2>
                    <@form.form commandName="addressForm" action="/cabinet/refactor/add_address" method="post">
                        <p><@form.input path="area" placeholder="Регион" size="40" />
                            <@form.errors path="area" cssStyle="color: red; font-size: 10px" /></p>

                        <p><@form.input path="city" placeholder="Город" size="40"/>
                            <@form.errors path="city" cssStyle="color: red; font-size: 10px" /></p>

                        <p><@form.input path="street" placeholder="Улица" size="40"/>
                            <@form.errors path="street" cssStyle="color: red; font-size: 10px" /></p>

                        <p><@form.input path="house" placeholder="Дом" size="40"/>
                            <@form.errors path="house" cssStyle="color: red; font-size: 10px" /></p>

                        <p><@form.input path="flat" placeholder="Квартира" size="40"/>
                            <@form.errors path="flat" cssStyle="color: red; font-size: 10px" /></p>

                        <p><@form.input path="index" placeholder="Индекс" size="40"/>
                            <@form.errors path="index" cssStyle="color: red; font-size: 10px" /></p>
                        <button type="submit" class="btn btn-default">Добавить</button>
                    </@form.form>
                </div>
                <!--/sign up form-->
            </div>
        </div>
    </div>
</section><!--/form-->
</#macro>