<#-- @ftlvariable name="ordersList" type="java.util.List<ru.kpfu.itis.shop.model.Orders>" -->
<#-- @ftlvariable name="addresses" type="java.util.List<ru.kpfu.itis.shop.model.Address>" -->
<#assign form=JspTaglibs["http://www.springframework.org/tags/form"]>
<#include "../templates/main_template.ftl">
<@main_template title="Личный кабинет" scripts=["cabinet.js"]/>

<#macro body>
<section id="cart_items">
    <div class="container">
        <p><a href="/cabinet/refactor">Редактировать</a></p>

        <p>Имя: ${(name)!}</p>

        <p>Логин: ${(email)!}</p>
        <#if addresses?has_content>
            <p><b>Адреса</b></p>
            <#list addresses as address>
                <div id="address_${(address.getId())!}">
                    <li><p>${(address.getArea())!}, ${(address.getCity())!}, ${(address.getStreet())!},
                        <#if address.getFlat()??>
                        ${(address.getHouse())!} - ${(address.getFlat())!},
                        <#else>
                        ${(address.getHouse())!},
                        </#if>
                        индекс: ${(address.getIndex())!}
                        <a href="" data-id="${(address.getId())!}" id="deleteAddress">Удалить</a>
                    </p></li>
                </div>
            </#list>
        <#else>У вас пока нет адресов.<a href="/cabinet/refactor"> Добавить</a>
        </#if>
        <hr>

        <#if ordersList?has_content>
            <#list ordersList as order>
            <#--блок всего заказа целиком-->
                <div id="order_${(order.getId())!}" class="order-info">
                <#--инфа о заказе-->
                    <div class="order-head">
                        <button id="cancel" data-id="${(order.getId())!}"
                                class="btn btn-default add-to-cart button-in-order-block">
                            Отменить заказ
                        </button>
                        <p>Время заказа: ${(order.getCreate_time())!}</p>
                        <p>Общая сумма заказа: ${(order.getTotal_sum())!}</p>
                        <p>Общее количество товара: ${(order.getTotal_count())!}</p>
                        <p>Статус заказа: ${(order.getStatus())!}</p>
                        <p>Адрес: ${(order.getAddress().getArea())!}, ${(order.getAddress().getCity())!}
                            , ${(order.getAddress().getStreet())!},
                        ${(order.getAddress().getHouse())!} - ${(order.getAddress().getFlat())!}
                            , ${(order.getAddress().getIndex())!}</p>
                    </div>

                    <div class="table-responsive cart_info">
                        <table class="table table-condensed">
                            <thead>
                            <tr class="cart_menu">
                                <td class="image">Товар</td>
                                <td class="description"></td>
                                <td class="price">Цена</td>
                                <td class="quantity">Кол-во</td>
                                <td class="total">Сумма</td>
                            </tr>
                            </thead>
                            <tbody>
                            <#--список заказанных книг-->
                                <#if order.getOrder_goods()?has_content>
                                    <#list order.getOrder_goods() as book>
                                    <tr>
                                        <td class="cart_product">
                                            <a href="/catalog/book/${(book.getGoods().getId())!}">
                                                <img src="${(book.getGoods().getImage())!}" alt=""></a>
                                        </td>
                                        <td class="cart_description">
                                            <h4>${(book.getGoods().getName())!}</h4>

                                            <p>${(book.getGoods().getAuthor())!}</p>

                                            <p>Жанр: ${(book.getGoods().getCategory().getAlias())!}</p>
                                        </td>
                                        <td class="cart_price">
                                            <p>${(book.getGoods().getPrice())!} руб</p>
                                        </td>
                                        <td class="cart_quantity">
                                                 <p>${(book.getCount())!}</p>
                                        </td>
                                        <td class="cart_total">
                                            <p class="cart_total_price">
                                <span id="total_price_${(book.getGoods().getId())!}">
                                    <#assign total_sum=book.getGoods().getPrice()*book.getCount()>
                                    ${total_sum}
                                </span></p>
                                        </td>
                                    </tr>
                                    </#list>
                                </#if>

                            </tbody>
                        </table>
                    </div>
                </div>
            </#list>
        <#else> <p align="center">Здесь будут отображены ваши заказы.</p>
        </#if>

    </div>
</section> <!--/#cart_items-->
</#macro>