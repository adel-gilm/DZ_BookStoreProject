<#-- @ftlvariable name="sortingFilter" type="ru.kpfu.itis.shop.util.SortingFilter" -->
<#-- @ftlvariable name="categories" type="java.util.List<ru.kpfu.itis.shop.model.Category>" -->
<div class="sorting-block">
    <form action="/catalog/sort" method="post">
        <p>Категории:</p>
        <label>
            <select name="category">
                <option value="0" <#if sortingFilter.getCategory()==0>selected</#if>>Все</option>
                <#if categories?has_content>
                    <#list categories as category>
                        <option value="${(category.getId())!}" <#if sortingFilter.getCategory()==category.getId()>selected</#if>>${category.getAlias()}</option>
                    </#list>
                </#if>
            </select>
        </label>
        <hr>

        <p>Сортировать по цене:</p>
        <label>
            <select name="sort">
                <option value="asc" <#if sortingFilter.getSort()=="asc">selected</#if>>Низкая-Высокая</option>
                <option value="desc" <#if sortingFilter.getSort()=="desc">selected</#if>>Высокая-Низкая</option>
            </select>
        </label>
        <hr>

        <p>Диапозон цен</p>
        <p>от: <input type="text" name="from" size="5" <#if sortingFilter.getFrom()!=0>
                      value="${sortingFilter.getFrom()}"</#if>></p>
        <p>до: <input type="text" name="to" size="5"<#if sortingFilter.getTo()!=0>
                      value="${sortingFilter.getTo()}"</#if>></p>
        <button type="submit" class="btn btn-block btn-primary">Найти</button>
    </form>

</div>