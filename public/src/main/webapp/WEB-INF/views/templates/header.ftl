<#assign sec=JspTaglibs["http://www.springframework.org/security/tags"]>

<header id="header"><!--header-->
    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="/"><img src="/resources/images/logo.png" width="125" height="50" alt=""/></a>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                        <@sec.authorize ifAnyGranted="ROLE_ANONYMOUS">
                            <li><a href="/cabinet"><i class="fa fa-lock"></i>Вход</a></li>
                        </@sec.authorize>
                        <#-- Если уже авторизован, то ссылка на выход -->
                        <@sec.authorize access="isAuthenticated()">
                            <li><a href="/cabinet"><i class="fa fa-user"></i>Личный кабинет</a></li>
                            <li><a href="/logout"><i class="fa fa-lock"></i> Выход</a></li>
                        </@sec.authorize>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                    <div class="mainmenu">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="/catalog">Каталог</a></li>
                            <li><a href="/cart">Корзина</a></li>
                        </ul>
                    </div>
            </div>
        </div>
    </div>
    <!--/header-bottom-->
</header>
<!--/header-->