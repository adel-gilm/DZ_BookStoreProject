<#-- @ftlvariable name="bookList" type="java.util.List<ru.kpfu.itis.shop.model.Cart>" -->
<#-- @ftlvariable name="addresses" type="java.util.List<ru.kpfu.itis.shop.model.Address>" -->
<#include "templates/main_template.ftl">
<@main_template title="Корзина" scripts=["cart.js"]/>

<#macro body>
<section id="cart_items">
<div class="container">
    <#if bookList?has_content>
    <#--кнопочка оформления заказа-->
        <#if bookList?has_content>
            <button type="submit" id="order" class="btn btn-default add-to-cart">
                Оформить заказ
            </button>

            <div id="hidden-block">
                <#if addresses?has_content>
                    <form action="/cart/order_processing" method="POST">
                        <p>Выберите адрес</p>
                        <select name="address">
                            <#list addresses as address>
                                <option value="${address.getId()}">${address.getArea()}, ${address.getCity()}, ${address.getStreet()},
                                ${address.getHouse()} - ${address.getFlat()}, ${address.getIndex()}</option>
                            </#list>
                        </select>
                        <button type="submit" class="btn btn-default add-to-cart" style="margin-left: 852px">Заказать</button>
                    </form>

                <#else>Нет адреса для отправки. Зайдите в личный кабинет и добавьте адрес.
                </#if>
            </div>
        </#if>
        <p align="right">Сумма: <span id="sum">${sum!0}</span> (<span id="amount">${amount!0}</span> товаров)</p>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                <tr class="cart_menu">
                    <td class="image">Товар</td>
                    <td class="description"></td>
                    <td class="price">Цена</td>
                    <td class="quantity">Кол-во</td>
                    <td class="total">Сумма</td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                    <#list bookList as cart>
                    <tr>
                        <td class="cart_product">
                            <a href="/catalog/book/${(cart.getGoods().getId())!}">
                                <img src="${(cart.getGoods().getImage())!}" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4>${(cart.getGoods().getName())!}</h4>

                            <p>${(cart.getGoods().getAuthor())!}</p>

                            <p>Жанр: ${(cart.getGoods().getCategory().getAlias())!}</p>
                        </td>
                        <td class="cart_price">
                            <p>${(cart.getGoods().getPrice())!} руб</p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <a id="plus" data-id="${(cart.getGoods().getId())!}" class="cart_quantity_up " href=""> + </a>
                                <a><span id="${(cart.getGoods().getId())!}">${(cart.getCount())!}</span></a>
                                <a id="mines" data-id="${(cart.getGoods().getId())!}" class="cart_quantity_down" href=""> - </a>
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">
                                <span id="total_price_${(cart.getGoods().getId())!}">
                                    <#assign total_sum=cart.getGoods().getPrice()*cart.getCount()>
                                    ${total_sum}
                                </span></p>
                        </td>
                        <td class="cart_delete">
                            <form action="/cart/action/${(cart.getGoods().getId())!}" method="post">
                                <input type="submit" name="delete" class="btn btn-default add-to-cart cart_quantity_delete" value="x">
                            </form>
                        </td>
                    </tr>
                    </#list>
                </tbody>
            </table>
        </div>
    <#else><p align="center">Ваша корзина пуста. <a href="/catalog">Перейти в каталог.</a></p>
    </#if>


   </div>
</section>


</#macro>
