<#include "templates/main_template.ftl">
<@main_template title="Добро пожаловать"/>

<#macro body>
<section id="slider"><!--slider-->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="carousel" class="carousel slide slider-block" data-ride="carousel" data-interval="4000">
                    <!-- Слайды карусели -->
                    <div class="carousel-inner ">
                        <div class="item active">
                            <img src="/resources/images/slider/1.JPG" width="800" alt="BookStore"/>
                        </div>
                        <div class="item">
                            <img src="/resources/images/slider/2.JPG" width="800" alt="BookStore"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</#macro>