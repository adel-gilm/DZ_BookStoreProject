<#-- @ftlvariable name="books" type="java.util.List<ru.kpfu.itis.shop.model.Goods>" -->
<#include "../templates/main_template.ftl">
<@main_template title="Каталог" scripts=["catalog.js"]/>

<#macro body>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Фильтрация</h2>
                    <#include "../templates/sorting_block.ftl">
                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Книги</h2>

                    <ul id="goodList">
                        <#include "goodItem.ftl">
                        <#if books?has_content>
                            <#list books as book>
                                <@goodItem book=book />
                            </#list>
                        <#else><p>Каталог пуст</p>
                        </#if>
                    </ul>

                </div>
            </div>
            <#if !end??>
                <a id="showMore" class="btn btn-default add-to-cart" style="margin-left: 500px">Показать еще</a>
            </#if>
        </div>
    </div>
</section>
</#macro>