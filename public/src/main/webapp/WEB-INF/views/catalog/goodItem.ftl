<#-- @ftlvariable name="cart" type="ru.kpfu.itis.shop.util.CartList" -->
<#-- @ftlvariable name="book" type="ru.kpfu.itis.shop.model.Goods" -->
<#macro goodItem book >

<div class="col-sm-4">
    <div class="productinfo text-center">
        <a href="/catalog/book/${(book.getId())!}">
            <img src=${(book.getImage())!} height="250" /></a>

        <h2>${(book.getPrice())!} руб</h2>
        <p>${(book.getName())!}</p>
        <p>${(book.getAuthor())!}</p>

    <#--Проверка есть ли в корзине содержимое, и сожержится ли этот товар в корзине-->
        <#if cart?? && cart.contains(book)>
            <a href="/cart" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>В корзине</a>
        <#else>
            <a data-id="${(book.getId())!}" href="/cart" class="btn btn-default add-to-cart js_addToCart"><i
                    class="fa fa-shopping-cart"></i>Добавить</a>
        </#if>
    </div>
</div>
</#macro>