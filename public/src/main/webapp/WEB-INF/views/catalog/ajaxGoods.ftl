<#include "goodItem.ftl">
<#if books?has_content>
<#list books as book>
    <@goodItem book=book  />
</#list>
</#if>