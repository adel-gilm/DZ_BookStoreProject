<#assign form=JspTaglibs["http://www.springframework.org/tags/form"]>
<#include "templates/main_template.ftl">
<@main_template title="Вход | Регистрация" scripts=["registration.js"]/>

<#macro body>
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form"><!--login form-->
                    <h2>Вход</h2>

                    <form name="authForm" id="authForm" action="/j_spring_security_check" method="POST" role="form">
                        <#if error?has_content>
                            <div style="text-align: right;color: red; font-size: 13px"><i>Неверный логин или пароль.</i>
                            </div>
                        </#if>
                        <input type="text" name="j_username" placeholder="Логин" required/>
                        <input type="password" name="j_password" placeholder="Пароль" required/>
							<span>
								<input type="checkbox" class="checkbox" id="remember_me"
                                       name="_spring_security_remember_me">
								Запомнить меня
							</span>
                        <button type="submit" class="btn btn-default">Вход</button>
                    </form>

                </div>
                <!--/login form-->
            </div>

            <div class="col-sm-1">
                <h2 class="or">Или</h2>
            </div>

            <div class="col-sm-4">
                <div class="signup-form"><!--sign up form-->
                    <h2>Регистрация</h2>

                    <@form.form commandName="form" action="/registration" method="post">
                       <p><@form.input path="userName" placeholder="Имя" onblur="validateName(this)" size="40"/>
                        <@form.errors path="userName" cssStyle="color: red; font-size: 10px" /></p>

                        <p><@form.input path="login" id="login" placeholder="Логин" onblur="validateEmail(this)" oninput="checkLogin(this)" size="40"/>
                        <@form.errors path="login" cssStyle="color: red; font-size: 10px" /></p>

                        <p><@form.password path="password" id="pass" placeholder="Пароль" onblur="validatePassword(this)" size="40"/>
                        <@form.errors path="password" cssStyle="color: red; font-size: 10px" /></p>

                        <p><@form.password path="confirmPassword" id="pass2" placeholder="Повторный пароль" onblur="validateConfirmPassword(this)" size="40"/>
                        <@form.errors path="confirmPassword" cssStyle="color: red; font-size: 10px" /></p>

                        <button type="submit" class="btn btn-default">Зарегистрироваться</button>
                    </@form.form>


                </div>
                <!--/sign up form-->
            </div>
        </div>
    </div>
</section><!--/form-->
</#macro>