/**
 * Created by Adel on 08.03.2016.
 */

$(document).ready(function () {

    $(document).on('click', '#cancel', function () {
        event.preventDefault();
        var $this = $(this);

        var isAdmin = confirm("Вы действительно хотите отменить заказ?");
        if (isAdmin) {
        $.ajax({
            type: 'POST',
            url: '/cabinet/deleteOrder',
            data: {id: $this.data('id')},
            success: function (data) {
                if (data == 'ok') {
                    $('#order_' + $this.data('id')).text("");
                }
                if (data == 'no') {
                    alert("К сожалению, ваш заказ не может быть отменен.")
                }
            },
            error: function () {
                alert('Приносим извинения. На сервере произошла ошибка');
            }
        });
    }
    });

    $(document).on('click', '#deleteAddress', function () {
        event.preventDefault();
        var $this = $(this);
        $.ajax({
            type: 'POST',
            url: '/cabinet/deleteAddress',
            data: {id: $this.data('id')},
            success: function (data) {
                if (data == 'ok') {
                    $('#address_' + $this.data('id')).text("");
                    alert("Адрес удален.")
                }
                if (data == 'no') {
                    alert("К сожалению, ваш адрес не может быть удален.")
                }
            },
            error: function () {
                alert('Приносим извинения. На сервере произошла ошибка');
            }
        });
    });
});