/**
 * Created by Adel on 19.03.2016.
 */

function checkLogin(input) {
    if ($("#login").val().length > 0)
        $.ajax({
            type: 'POST',
            url: '/registration/checkingLogin',
            data: {login: $('#login').val()},
            success: function (data) {
                if (data == 'ok') {
                    $('#login').val('');
                    alert("К сожалению, такой логин уже существует.");
                }
            },
            error: function () {
                alert('Приносим извинения. На сервере произошла ошибка');
            }
        });
}

function regFunc() {
    alert("Для окончания регистрации пройдите по ссылке, что мы выслали вам на почту");
}
function validateName(input) {
    var result = input.value.match(/.+/);
    if (input.value == result) {
        input.setCustomValidity("");
    }
    else {
        input.setCustomValidity("Неверный формат");
    }
}
function validateAddress(input) {
    var result = input.value.match(/[\D\s]+, [\D\s]+, [\D\s]+, [\d]{1,5} - [\d\D]{1,5}, \d{6}/);
    if (input.value == result) {
        input.setCustomValidity("");
    }
    else {
        input.setCustomValidity("Неверный формат");
    }
}
function validateEmail(input) {
    var result = input.value.match(/.+@.+\.[A-Za-z]{2,4}/);
    if (input.value == result) {
        input.setCustomValidity("");
    }
    else {
        input.setCustomValidity("Неверный формат");
    }
}
function validatePassword(input) {
    var result = input.value.match(/.{3,8}/);
    if (input.value == result) {
        input.setCustomValidity("");
    }
    else {
        input.setCustomValidity("Пароль должен быть от 3 до 8 символов");
    }
}
function validateConfirmPassword(input) {
    var pass = $('#pass').val();
    var pass2 = $('#pass2').val();
    if (pass == pass2) {
        input.setCustomValidity("");
    }
    else {
        input.setCustomValidity("Пароли не совпадают. Повторите попытку");
    }
}

