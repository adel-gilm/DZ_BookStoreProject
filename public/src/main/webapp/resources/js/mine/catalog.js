/**
 * Created by Adel on 07.03.2016.
 */

$(document).ready(function () {

    $(document).on('click', '.js_addToCart', function () {
        event.preventDefault();
        var $this = $(this);
        $.ajax({
            type: 'POST',
            url: '/catalog/addBook/' + $this.data('id'),
            data: {id: $this.data('id')},
            success: function (data) {  // успешное завершение работы
                //console.log('/cart/increase result: data=' + data + '; status=' + status);
                if (data == 'ok') {
                    $this.removeClass('js_addToCart').text('В корзине');
                }
            },
            error: function () {    // На сервере произошла ошибка
                alert('Приносим извинения. На сервере произошла ошибка');
            }
        });
    });

    $(document).on('click', '#showMore', function () {
        var $this = $(this);
        $.ajax({
            type: "POST",
            url: "/catalog/showMore"
        }).done(function (data) {  // сюда приходит ответ при успехе
            if (data != '') {
                $("#goodList").append(data);
            } else {
                $this.hide();
            }
        }).fail(function () {      // сюда приходит ответ если на сервере прооизошла ошибка
            $this.hide();
        });

    });



    //до лучших времен
    //search = function(request, response){
    //    event.preventDefault();
    //    var $this = $(this);
    //    if ($("#s").val().length > 0)
    //        $.ajax({
    //            type: 'POST',
    //            url: '/catalog/searching',
    //            data: {name: $('#s').val()},
    //            dataType: "json",
    //            success: function (data) {
    //                if (data.results.length > 0) {
    //                    //$("#res").text("");
    //                    //for (var i = 0; i < data.results.length; i++) {
    //                    //    $("#res").append("<div class='delete-block'> " +
    //                    //        "<form action='/cook' method='post'> " +
    //                    //        "<input type='hidden' name='delete' value='"+msg.results[i].order_content+"'>" +
    //                    //        "<input type='submit' value='Удалить'> </form>" +
    //                    //        "</div>");
    //                    //    $("#res").append("<li>" + msg.results[i].cl + "<br>Время: "+ msg.results[i].order_time +
    //                    //        "<br>Дата: " +  msg.results[i].order_date + "<br>Заказ: " + msg.results[i].order_content +
    //                    //        "<br></li><hr>");
    //                    //}
    //                } else {
    //                    alert("По вышему запросу ничего не найдено");
    //                }
    //            },
    //            error: function () {    // На сервере произошла ошибка
    //                alert('Приносим извинения. На сервере произошла ошибка');
    //            }
    //        });
    //    //возможно тут придется весь каталог возвращать
    //    //else
    //    //    $("#res").text("");
    //}


});