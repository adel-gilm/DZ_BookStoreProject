/**
 * Created by Adel on 07.03.2016.
 */

$(document).ready(function () {

    $(document).on('click', '#plus', function () {
        event.preventDefault();
        var $this = $(this);
        $.ajax({
            type: 'POST',
            url: '/cart/increase/' + $this.data('id'),
            data: {id: $this.data('id')},
            success: function (data) {  // успешное завершение работы
                var $amountOfGood = $('#' + $this.data('id'));
                var amountOfGood = parseInt($amountOfGood.text());
                $amountOfGood.text(amountOfGood + 1);
                var $amount = $('#amount');
                $amount.text(parseInt($amount.text()) + 1);
                var $sum = $('#sum');
                $sum.text(parseInt($sum.text()) + parseInt(data));
                var $total_price = $("#total_price_" + $this.data('id'));
                $total_price.text(parseInt($total_price.text()) + parseInt(data));
            },
            error: function () {    // На сервере произошла ошибка
                alert('Приносим извинения. На сервере произошла ошибка');
            }
        });
    });

    $(document).on('click', '#mines', function () {
        event.preventDefault();
        var $this = $(this);
        $.ajax({
            type: 'POST',
            url: '/cart/remove/' + $this.data('id'),
            data: {id: $this.data('id')},
            success: function (data) {  // успешное завершение работы
                //console.log('/cart/increase result: data=' + data + '; status=' + status);
                var $amountOfGood = $('#' + $this.data('id'));
                var amountOfGood = parseInt($amountOfGood.text());
                if (amountOfGood > 1) {
                    $amountOfGood.text(amountOfGood - 1);
                    var $amount = $('#amount');
                    $amount.text(parseInt($amount.text()) - 1);
                    var $sum = $('#sum');
                    $sum.text(parseInt($sum.text()) - parseInt(data));
                    var $total_price = $("#total_price_" + $this.data('id'));
                    $total_price.text(parseInt($total_price.text()) - parseInt(data));
                }
            },
            error: function () {    // На сервере произошла ошибка
                alert('Приносим извинения. На сервере произошла ошибка');
            }
        });
    });

    $(document).on('click', '#order', function () {
        document.getElementById("hidden-block").style.display = 'inline';
        document.getElementById("order").style.display = 'none';
    });

});