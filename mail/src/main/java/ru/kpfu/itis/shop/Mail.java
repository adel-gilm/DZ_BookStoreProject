package ru.kpfu.itis.shop;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Adel on 15.03.2016.
 */
public class Mail {

    public void registrationText(String userName, String email, String key) throws IOException {
        String text = "Привет, " + userName + "!\n" +
                "\n" +
                "Пройдите по ссылке для окончания регистрации:\n" +
                "http://www.localhost:8080/registration/verification/" + email + "/" + key + " \n" +
                "\n" +
                "Если вы не регистрировались на нашем сайте, то проигнорируйте это сообщение.";
        sendMail(email, text);
    }

    public void loginChangingText(String userName, String email, String newLogin) throws IOException {
        String text = "Hi, " + userName + "!\n" +
                "\n" +
                "Уведомляем вас о том, что ваш логин был изменен.\n" +
                "\n" +
                "Ваш новый логин: " + newLogin;
        sendMail(email, text);
    }

    public void passwordChangingText(String userName, String email, String newPass) throws IOException {
        String text = "Hi, " + userName + "!\n" +
                "\n" +
                "Уведомляем вас о том, что ваш пароль был изменен.\n" +
                "\n" +
                "Ваш новый пароль: " + newPass;
        sendMail(email, text);
    }

    public void sendMail(String email, String text) throws IOException {
        Properties props = new Properties();
        String propFileName = "mail.properties";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        props.load(inputStream);
        final String username = props.getProperty("username");
        final String password = props.getProperty("password");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email));
            message.setSubject("BookStore");
            message.setText(text);

            Transport.send(message);
        } catch (MessagingException e) {
            //throw new RuntimeException(e);
        }
    }
}
