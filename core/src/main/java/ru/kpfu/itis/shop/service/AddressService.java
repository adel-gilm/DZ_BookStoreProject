package ru.kpfu.itis.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.shop.model.Address;
import ru.kpfu.itis.shop.model.Orders;
import ru.kpfu.itis.shop.repository.AddressRepository;
import ru.kpfu.itis.shop.repository.OrdersRepository;

import java.util.List;

/**
 * Created by Adel on 02.04.2016.
 */
@Service
public class AddressService {
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private OrdersRepository ordersRepository;

    @Transactional
    public List<Address> getAddress(Long id) {
        return addressRepository.getAddress(id);
    }

    @Transactional
    public boolean deleteAddress(Long addressId, Long userId) {
        List<Orders> list = ordersRepository.getOrders(userId);
        for (Orders order : list) {
            //���� �� ����� ������ ��� ������ �����, ������� ��� ������
            if (order.getAddress().getId().equals(addressId)) {
                return false;
            }
        }
        addressRepository.deleteAddress(addressId, userId);
        return true;
    }

    @Transactional
    public void addAddress(String area, String city, String street, String house, String flat, String index, Long userId) {
        if (flat == null || flat.isEmpty())
            addressRepository.addAddress(area, city, street, house, null, Integer.parseInt(index), userId);
        else addressRepository.addAddress(area, city, street, house, Integer.parseInt(flat), Integer.parseInt(index), userId);
    }
}
