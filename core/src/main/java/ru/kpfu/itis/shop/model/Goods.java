package ru.kpfu.itis.shop.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Adel on 23.11.2015.
 */
@Entity
@Table(name = "Goods")
public class Goods {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column
    private String author;

    @Column
    private Date year;

    @Column
    private String country;

    @Column
    private Double price;

    @Column
    private Integer count;

    @Column
    private Double size;

    @Column
    private String image;

    @Column
    private String discription;

    @Column
    private String color;

    @ManyToOne
            (cascade = {CascadeType.REFRESH},
                    fetch = FetchType.LAZY)
    @JoinColumn(name = "category")
    private Category category;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy = "goods")
    private List<Cart> cart;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy = "goods")
    private List<Order_Goods> order_goods;

    @Override
    public String toString() {
        return "Goods{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                ", image='" + image + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public Date getYear() {
        return year;
    }

    public String getCountry() {
        return country;
    }

    public Double getPrice() {
        return price;
    }

    public Integer getCount() {
        return count;
    }

    public Double getSize() {
        return size;
    }

    public String getImage() {
        return image;
    }

    public String getDiscription() {
        return discription;
    }

    public String getColor() {
        return color;
    }

    public Category getCategory() {
        return category;
    }

    public List<Cart> getCart() {
        return cart;
    }

    public List<Order_Goods> getOrder_goods() {
        return order_goods;
    }

}
