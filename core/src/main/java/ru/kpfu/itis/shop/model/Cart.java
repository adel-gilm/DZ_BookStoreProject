package ru.kpfu.itis.shop.model;

import javax.persistence.*;

/**
 * Created by Adel on 23.11.2015.
 */
@Entity
@Table(name="Cart")
public class Cart {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
            (cascade = {CascadeType.REFRESH},
                    fetch = FetchType.LAZY)
    @JoinColumn(name = "goods")
    private Goods goods;

    @ManyToOne
            (cascade = {CascadeType.REFRESH},
                    fetch = FetchType.LAZY)
    @JoinColumn(name = "users")
    private Users users;

    @Column
    private Long count;

    public Cart() {    }

    public Cart(Goods goods, Long count) {
        this.goods = goods;
        this.count = count;
    }

    public Cart(Long count, Users users, Goods goods) {
        this.count = count;
        this.users = users;
        this.goods = goods;
    }

    public Goods getGoods() {
        return goods;
    }

    public Users getUsers() {
        return users;
    }

    public Long getCount() {
        return count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public void setCount(Long count) {
        this.count = count;
    }

}
