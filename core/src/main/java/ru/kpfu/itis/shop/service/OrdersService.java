package ru.kpfu.itis.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.shop.model.Orders;
import ru.kpfu.itis.shop.repository.OrdersRepository;

import java.util.List;

/**
 * Created by Adel on 06.03.2016.
 */
@Service
public class OrdersService {
    @Autowired
    private OrdersRepository ordersRepository;

    /*
     * Получение списка заказов определенного пользователя
     */
    @Transactional
    public List<Orders> getOrders(Long id) {
        return ordersRepository.getOrders(id);
    }

    /*
     * Отмена заказа пользователя, если это возможно
     */
    @Transactional
    public boolean orderDeletion(long id) {
        return ordersRepository.orderDeletion(id);
    }

    /*
     * Добавление нового заказа дял пользователя
     */
    @Transactional
    public void orderAddition(Long userId, long addressId) {
        ordersRepository.orderAddition(userId, addressId);
    }

}
