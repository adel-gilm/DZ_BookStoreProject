package ru.kpfu.itis.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.shop.model.Goods;
import ru.kpfu.itis.shop.repository.GoodsRepository;

import java.util.List;

@Service
public class GoodsService {
    @Autowired
    private GoodsRepository goodsRepository;

    @Transactional
    public List<Goods> getAllGoods() {
        return goodsRepository.getAllGoods();
    }

    @Transactional
    public Goods getBook(long id) {
        return goodsRepository.getBook(id);
    }

    @Transactional
    public List<Goods> getSortingGoods(String categoryId, String sort, Double min, Double max) {
        List<Goods> list = goodsRepository.getSortingGoods(sort); //все отсортированные книги
        //поиск по категориям
        if (categoryId != null) {
            if (Long.parseLong(categoryId) != 0)
                list = goodsRepository.getGoodsInCategory(Long.parseLong(categoryId), list);
        }
        //поиск по цене
        list = goodsRepository.getGoodsInPriceFromTo(min, max, list);
        return list;
    }
}
