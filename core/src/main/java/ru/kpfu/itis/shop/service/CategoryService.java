package ru.kpfu.itis.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.shop.model.Category;
import ru.kpfu.itis.shop.repository.CategoryRepository;

import java.util.List;

/**
 * Created by Adel on 14.04.2016.
 */
@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Transactional
    public List<Category> getAllCategories() {
        return categoryRepository.getAllCategories();
    }
}
