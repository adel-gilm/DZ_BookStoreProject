package ru.kpfu.itis.shop.repository;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.shop.model.Goods;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Repository
public class GoodsRepository {
    @Autowired
    private SessionFactory sessionFactory;

    /*
     * Получение всех товаров для каталога
     */
    @SuppressWarnings("unchecked")
    public List<Goods> getAllGoods() {
        return sessionFactory.getCurrentSession().createCriteria(Goods.class).list();
    }

    /*
     * Получение книжки по ее id
     */
    public Goods getBook(long id) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Goods.class);
        return (Goods) crit.add(Restrictions.eq("id", id)).uniqueResult();
    }

    /*
     * Возвращает отсортированный по возрастанию или убыванию книжки
     */
    @SuppressWarnings("unchecked")
    public ArrayList<Goods> getSortingGoods(String sort) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Goods.class);
        if (sort.equals("asc"))
            crit.addOrder(org.hibernate.criterion.Order.asc("price"));
        else
            crit.addOrder(org.hibernate.criterion.Order.desc("price"));
        return (ArrayList<Goods>) crit.list();
    }

    /*
     * Возвращает список книг нужной категори, путем отсеивания из сущ-его неподходящих книг
     */
    public List<Goods> getGoodsInCategory(Long categoryId, List<Goods> list) {
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            Goods good = (Goods) iterator.next();
            if (!good.getCategory().getId().equals(categoryId))
                iterator.remove();
        }
        return list;
    }

    /*
     * Возвращает книги в нужном ценовом диапозоне
     */
    public List<Goods> getGoodsInPriceFromTo(Double min, Double max, List<Goods> list) {
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            Goods good = (Goods) iterator.next();
            if (good.getPrice() < min || good.getPrice() > max) {
                iterator.remove();
            }
        }
        return list;
    }

}
