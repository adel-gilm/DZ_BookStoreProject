package ru.kpfu.itis.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.shop.model.Cart;
import ru.kpfu.itis.shop.model.Goods;
import ru.kpfu.itis.shop.repository.CartRepository;

import java.util.List;

/**
 * Created by Adel on 29.02.2016.
 */
@Service
public class CartService {
    @Autowired
    private CartRepository cartRepository;

    @Transactional
    public List<Cart> getAnonBooks(String bookList){
        return cartRepository.getAnonBooks(bookList);
    }

    @Transactional
    public List<Cart> getAllGoodsFromCart(Long userId){
        return cartRepository.getAllGoodsFromCart(userId);
    }

    @Transactional
    public void addAllBooksFromSession(String books, Long userId) {
        cartRepository.addAllBooksFromSession(books, userId);
    }

    @Transactional
    public void deleteBookInCart(Long userId, long bookId) {
        cartRepository.deleteBookInCart(userId, bookId);
    }

    @Transactional
    public void addBook(Long userId, long bookId) {
        cartRepository.addBook(userId, bookId);
    }

    @Transactional
    public void increaseBook(Long userId, long id) {
        cartRepository.increaseBook(userId, id);
    }

    @Transactional
    public void removeBook(Long userId, Long id) {
        cartRepository.removeBook(userId, id);
    }

    @Transactional
    public List<Goods> getListOfBooksFromCart(Long userId) {
        return cartRepository.getListOfBooksFromCart(userId);
    }

    @Transactional
    public List<Goods> getListOfAnonBooks(String bookList) {
        return cartRepository.getAnonListOfBooks(bookList);
    }
}
