package ru.kpfu.itis.shop.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Adel on 23.11.2015.
 */
@Entity
@Table(name="Order_Goods")
public class Order_Goods implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
            (cascade = {CascadeType.REFRESH},
                    fetch = FetchType.LAZY)
    @JoinColumn(name = "goods")
    private Goods goods;

    @ManyToOne
            (cascade = {CascadeType.REFRESH},
                    fetch = FetchType.LAZY)
    @JoinColumn(name = "orders")
    private Orders orders;

    public Order_Goods() {
    }

    public Order_Goods(Goods goods, Long count, Orders orders) {
        this.goods = goods;
        this.count = count;
        this.orders = orders;
    }



    @Column
    private Long count;

    public Goods getGoods() {
        return goods;
    }

    public Orders getOrders() {
        return orders;
    }

    public Long getCount() {
        return count;
    }
}
