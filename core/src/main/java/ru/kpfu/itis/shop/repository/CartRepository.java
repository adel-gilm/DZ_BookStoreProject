package ru.kpfu.itis.shop.repository;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.shop.model.Cart;
import ru.kpfu.itis.shop.model.Goods;
import ru.kpfu.itis.shop.model.Users;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Adel on 29.02.2016.
 */
@Repository
public class CartRepository {
    @Autowired
    private SessionFactory sessionFactory;

    /*
     * Получение списка книг для неавторизованного пользователя, не из бд
     */
    @SuppressWarnings("unchecked")
    public List<Cart> getAnonBooks(String bookList) {
        List<Cart> cartList = new LinkedList<>();
        if (bookList == null || bookList.equals("")) {
            return null;
        } else {
            String book[] = bookList.split(";");
            for (int i = 0; i < book.length; i++) {
                String amount[] = book[i].split("-");
                Criteria crit = sessionFactory.getCurrentSession().createCriteria(Goods.class);
                Goods goods = (Goods) crit.add(Restrictions.eq("id", Long.parseLong(amount[0]))).uniqueResult();
                Cart cart = new Cart(Long.parseLong(amount[1]), null, goods);
                cartList.add(cart);
            }
            return cartList;
        }
    }

    /*
     * Перенос книг из сессии в бд при авторизации
     */
    @SuppressWarnings("unchecked")
    public void addAllBooksFromSession(String bookList, Long userId) {
        Criteria userCrit = sessionFactory.getCurrentSession().createCriteria(Users.class);
        Users user = (Users) userCrit.add(Restrictions.eq("id", userId)).uniqueResult();//нахожу юзера
        if (bookList != null && !bookList.equals("")) {
            String book[] = bookList.split(";");
            for (int i = 0; i < book.length; i++) {
                String inf[] = book[i].split("-");
                Criteria goodsCrit = sessionFactory.getCurrentSession().createCriteria(Goods.class);
                Goods good = (Goods) goodsCrit.add(Restrictions.eq("id", Long.parseLong(inf[0]))).uniqueResult();//нахожу товар
                //в этом месте убираем дублирование товаров в корзине
                Criteria cartCrit = sessionFactory.getCurrentSession().createCriteria(Cart.class);
                Cart cart = (Cart) cartCrit.add(Restrictions.eq("goods.id", good.getId())).uniqueResult();
                if (cart != null) {
                    cart.setCount(cart.getCount() + Long.parseLong(inf[1]));
                    sessionFactory.getCurrentSession().update(cart);
                } else {
                    cart = new Cart(Long.parseLong(inf[1]), user, good);
                    sessionFactory.getCurrentSession().save(cart);
                }
            }
        }
    }

    /*
     * Получение товаров из корзины для пользователя с id
     */
    @SuppressWarnings("unchecked")
    public List<Cart> getAllGoodsFromCart(Long userId) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Cart.class);
        return crit.add(Restrictions.eq("users.id", userId)).list();
    }

    /*
     * Полное удаление книжки из корзины из бд
     */
    @SuppressWarnings("unchecked")
    public void deleteBookInCart(Long userId, long bookId) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Cart.class);
        Cart cart = (Cart) crit.
                add(Restrictions.eq("users.id", userId)).
                add(Restrictions.eq("goods.id", bookId)).uniqueResult();
        sessionFactory.getCurrentSession().delete(cart);
    }

    /*
     * Добавление книги в бд для авторизованного пользователя
     */
    @SuppressWarnings("uncheked")
    public void addBook(Long userId, Long id) {
        Criteria userCrit = sessionFactory.getCurrentSession().createCriteria(Users.class);
        Users user = (Users) userCrit.add(Restrictions.eq("id", userId)).uniqueResult();//нахожу юзера
        Criteria goodsCrit = sessionFactory.getCurrentSession().createCriteria(Goods.class);
        Goods good = (Goods) goodsCrit.add(Restrictions.eq("id", id)).uniqueResult();//нахожу товар

        //в этом месте убираем дублирование товаров в корзине
        Criteria cartCrit = sessionFactory.getCurrentSession().createCriteria(Cart.class);
        Cart cart = (Cart) cartCrit.add(Restrictions.eq("goods.id", good.getId())).uniqueResult();

        if (cart != null) {
            cart.setCount(cart.getCount() + 1);
            sessionFactory.getCurrentSession().update(cart);
        } else {
            cart = new Cart((long) 1, user, good);
            sessionFactory.getCurrentSession().save(cart);
        }
    }

    /*
     * Увеличение кол-ва книг на одну для авторизованного пользователя
     */
    @SuppressWarnings("uncheked")
    public void increaseBook(Long userId, long id) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Cart.class);
        Cart cart = (Cart) crit.add(Restrictions.eq("users.id", userId)).
                add(Restrictions.eq("goods.id", id)).uniqueResult();
        cart.setCount(cart.getCount() + 1);
        sessionFactory.getCurrentSession().update(cart);
    }

    /*
     * Уменьшение кол-ва книг на одну для авторизованного пользователя
     */
    @SuppressWarnings("uncheked")
    public void removeBook(Long userId, Long id) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Cart.class);
        Cart cart = (Cart) crit.add(Restrictions.eq("users.id", userId)).
                add(Restrictions.eq("goods.id", id)).uniqueResult();
        if (cart.getCount() > 1) {
            cart.setCount(cart.getCount() - 1);
            sessionFactory.getCurrentSession().update(cart);
        }
    }

    /*
     * Возвращает список объектов "книга", а не объектов "корзина" для авторизованного пользователя
     */
    @SuppressWarnings("uncheked")
    public List<Goods> getListOfBooksFromCart(Long userId) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Cart.class);
        List<Cart> cartList= crit.add(Restrictions.eq("users.id", userId)).list();
        List<Goods> goodsList= new ArrayList<>();
        for (Cart cart: cartList){
            goodsList.add(cart.getGoods());
        }
        return goodsList;
    }

    public List<Goods> getAnonListOfBooks(String bookList) {
        List<Cart> cartList = getAnonBooks(bookList);
        List<Goods> goodsList= new ArrayList<>();
        if(cartList!=null){
            for (Cart cart: cartList){
                goodsList.add(cart.getGoods());
            }
        }
        return goodsList;
    }
}
