package ru.kpfu.itis.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.shop.model.Users;
import ru.kpfu.itis.shop.repository.UsersRepository;

import java.security.NoSuchAlgorithmException;

/**
 * Created by Adel on 07.03.2016.
 */
@Service
public class UsersService {
    @Autowired
    private UsersRepository usersRepository;

    @Transactional
    public Users getUser(String login) {
        return usersRepository.getUser(login);
    }

    @Transactional
    public void addUser(String userName, String login, String hash_pass, String key) throws NoSuchAlgorithmException {
        usersRepository.addUser(userName, login, hash_pass, key);
    }

    /*
     * Получение ключа для проверки по логину
     */
    @Transactional
    public String getKey(String login) {
        return usersRepository.getUser(login).getKey();
    }

    @Transactional
    public void changeChecking(String login) {
        usersRepository.changeChecking(login);
    }

    /*
     * Проверка на уникальность логина при регистрации
     */
    @Transactional
    public String checkLogin(String login) {
        Users user = usersRepository.getUser(login);
        if (user != null) return "ok";  //если существует возвращает ok
        else return "no";
    }

    /*
     * Получение имени пользователя по логину
     */
    @Transactional
    public String getUserName(String login) {
        return usersRepository.getUser(login).getName();
    }

    /*
     * Изменение имени
     */
    @Transactional
    public void setName(String name, String login) {
        usersRepository.setName(name, login);
    }

    @Transactional
    public void setLogin(String login, String oldLogin) {
        usersRepository.setLogin(login, oldLogin);
    }

    @Transactional
    public void setPassword(String hash_pass, String login) {
        usersRepository.setPassword(hash_pass, login);
    }
}
