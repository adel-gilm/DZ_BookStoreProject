package ru.kpfu.itis.shop.repository;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.shop.model.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Adel on 06.03.2016.
 */
@Repository
public class OrdersRepository {
    @Autowired
    private SessionFactory sessionFactory;

    /*
     * Получение заказов пользователя
     */
    @SuppressWarnings("unchecked")
    public List<Orders> getOrders(Long id) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Orders.class);
        return crit.add(Restrictions.eq("users.id", id)).list();
    }

    /*
     * Удаление заказа при возможности
     */
    @SuppressWarnings("unchecked")
    public boolean orderDeletion(long id) {
        Criteria orderCrit = sessionFactory.getCurrentSession().createCriteria(Orders.class);
        Orders order = (Orders) orderCrit.add(Restrictions.eq("id", id)).uniqueResult();
        if (order.getStatus().equals("отправлено")) {
            return false;
        } else {
            Criteria order_goodsCrit = sessionFactory.getCurrentSession().createCriteria(Order_Goods.class);
            ArrayList<Order_Goods> order_good = (ArrayList<Order_Goods>) order_goodsCrit.add(Restrictions.eq("orders.id", id)).list();
            for (Order_Goods book : order_good) {
                sessionFactory.getCurrentSession().delete(book);
            }
            sessionFactory.getCurrentSession().delete(order);
            return true;
        }

    }

    /*
     * Добавление заказа в таблицу orders и orders_goods
     */
    @SuppressWarnings("unchecked")
    public void orderAddition(long userId, Long addressId) {
        double total_sum = 0;
        int total_count = 0;

        Criteria critAddress = sessionFactory.getCurrentSession().createCriteria(Address.class); //найден адрес
        Address address = (Address) critAddress.add(Restrictions.eq("id", addressId)).uniqueResult();
        Criteria critUser = sessionFactory.getCurrentSession().createCriteria(Users.class);
        Users user = (Users) critUser.add(Restrictions.eq("id", userId)).uniqueResult(); //найден юзер
        Orders order = new Orders(new Date(), "в обработке", null, address, user); //добавлен заказ
        sessionFactory.getCurrentSession().save(order);

        Criteria critCart = sessionFactory.getCurrentSession().createCriteria(Cart.class);
        ArrayList<Cart> cartList = (ArrayList<Cart>) critCart.add(Restrictions.eq("users.id", userId)).list();
        for (Cart cart : cartList) {
            total_count += cart.getCount();
            total_sum += cart.getGoods().getPrice() * cart.getCount();
            Order_Goods order_goods = new Order_Goods(cart.getGoods(), cart.getCount(), order);
            sessionFactory.getCurrentSession().save(order_goods);
            sessionFactory.getCurrentSession().delete(cart);
        }
        order.setTotal_count(total_count);
        order.setTotal_sum(total_sum);
        sessionFactory.getCurrentSession().update(order);

    }

}
