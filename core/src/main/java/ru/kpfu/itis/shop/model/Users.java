package ru.kpfu.itis.shop.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Adel on 23.11.2015.
 */
@Entity
@Table(name = "Users")
public class Users {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String login;

    @Column
    private String hash_pass;

    @Column
    private String avatar;

    @Column
    private String name;

    @Column
    private Boolean enabled;

    @Column
    private String key;

    @Column
    private String role;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy = "users")
    private List<Address> address;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy = "users")
    private List<Orders> orders;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy = "users")
    private List<Cart> cart;

    public Users() {
    }

    public Users(String login, String hash_pass, String name, String key, String role, Boolean enabled) {
        this.login = login;
        this.hash_pass = hash_pass;
        this.name = name;
        this.enabled = enabled;
        this.key = key;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getHash_pass() {
        return hash_pass;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public List<Address> getAddress() {
        return address;
    }

    public List<Orders> getOrders() {
        return orders;
    }

    public List<Cart> getCart() {
        return cart;
    }

    public String getRole() {
        return role;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setHash_pass(String hash_pass) {
        this.hash_pass = hash_pass;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public void setOrders(List<Orders> orders) {
        this.orders = orders;
    }

    public void setCart(List<Cart> cart) {
        this.cart = cart;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
