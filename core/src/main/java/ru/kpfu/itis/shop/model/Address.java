package ru.kpfu.itis.shop.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Adel on 23.11.2015.
 */
@Entity
@Table(name="Address")
public class Address {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String city;

    @Column
    private String street;

    @Column
    private String house;

    @Column
    private Integer flat;

    @Column
    private Integer index;

    @Column
    private String area;

    @ManyToOne
            (cascade = {CascadeType.REFRESH},
                    fetch = FetchType.LAZY)
    @JoinColumn(name = "users")
    private Users users;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy="address")
    private List<Orders> orders;

    public Address(String city, String area, String street, String house, Integer flat, Integer index, Users users) {
        this.city = city;
        this.area = area;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.index = index;
        this.users = users;
    }

    public Address() {
    }

    public Long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public Integer getFlat() {
        return flat;
    }

    public Integer getIndex() {
        return index;
    }

    public String getArea() {
        return area;
    }

    public Users getUsers() {
        return users;
    }

    public List<Orders> getOrders() {
        return orders;
    }
}
