package ru.kpfu.itis.shop.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Adel on 23.11.2015.
 */
@Entity
@Table(name = "Orders")
public class Orders {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Date create_time;

    @Column
    private Double total_sum;

    @Column
    private Integer total_count;

    @Column
    private String status;

    @Column
    private String pay_type;

    @ManyToOne
            (cascade = {CascadeType.REFRESH},
                    fetch = FetchType.LAZY)
    @JoinColumn(name = "address")
    private Address address;

    @ManyToOne
            (cascade = {CascadeType.REFRESH},
                    fetch = FetchType.LAZY)
    @JoinColumn(name = "users")
    private Users users;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy = "orders")
    private List<Order_Goods> order_goods;

    public Orders() {
    }

    public Orders(Date create_time, String status, String pay_type,
                  Address address, Users users) {
        this.create_time = create_time;
        this.status = status;
        this.pay_type = pay_type;
        this.address = address;
        this.users = users;
    }



    public Long getId() {
        return id;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public Double getTotal_sum() {
        return total_sum;
    }

    public Integer getTotal_count() {
        return total_count;
    }

    public String getStatus() {
        return status;
    }

    public String getPay_type() {
        return pay_type;
    }

    public Address getAddress() {
        return address;
    }

    public Users getUsers() {
        return users;
    }

    public List<Order_Goods> getOrder_goods() {
        return order_goods;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public void setTotal_sum(Double total_sum) {
        this.total_sum = total_sum;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public void setOrder_goods(List<Order_Goods> order_goods) {
        this.order_goods = order_goods;
    }
}
