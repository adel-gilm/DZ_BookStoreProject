package ru.kpfu.itis.shop.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Adel on 23.11.2015.
 */
@Entity
@Table(name="Category")
public class Category {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column
    private String alias;

    @Column
    private String parent_id;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy="category")
    private List<Goods> goods;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAlias() {
        return alias;
    }

    public List<Goods> getGoods() {
        return goods;
    }

    public String getParent_id() {
        return parent_id;
    }
}
