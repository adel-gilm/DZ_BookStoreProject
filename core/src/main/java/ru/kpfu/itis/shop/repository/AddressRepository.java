package ru.kpfu.itis.shop.repository;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.shop.model.Address;
import ru.kpfu.itis.shop.model.Users;

import java.util.List;

/**
 * Created by Adel on 02.04.2016.
 */
@Repository
public class AddressRepository {
    @Autowired
    private SessionFactory sessionFactory;

    /*
     * Получение адресов пользователя
     */
    @SuppressWarnings("unchecked")
    public List<Address> getAddress(Long id) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Address.class);
        return crit.add(Restrictions.eq("users.id", id)).list();
    }

    @SuppressWarnings("unchecked")
    public void deleteAddress(Long addressId, Long userId) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Address.class);
        Address address = (Address) crit.add(Restrictions.eq("id", addressId)).add(Restrictions.eq("users.id", userId)).uniqueResult();
        sessionFactory.getCurrentSession().delete(address);
    }

    @SuppressWarnings("unchecked")
    public void addAddress(String area, String city, String street, String house, Integer flat, Integer index, Long userId) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Users.class);
        Users user = (Users) crit.add(Restrictions.eq("id", userId)).uniqueResult();
        Address address = new Address(city, area, street, house, flat, index, user);
        sessionFactory.getCurrentSession().save(address);
    }
}
