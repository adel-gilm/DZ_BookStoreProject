package ru.kpfu.itis.shop.repository;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.shop.model.Users;

import java.security.NoSuchAlgorithmException;

/**
 * Created by Adel on 07.03.2016.
 */
@Repository
public class UsersRepository {
    @Autowired
    private SessionFactory sessionFactory;

    /*
     * Достает пользовалеля по логину
     */
    public Users getUser(String login) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Users.class);
        return (Users) crit.add(Restrictions.eq("login", login)).uniqueResult();
    }

    /*
     * Добавление нового пользователя в бд
     */
    public void addUser(String userName, String login, String hash_pass, String key) throws NoSuchAlgorithmException {
        Users user = new Users(login, hash_pass, userName, key, "ROLE_USER", false);
        sessionFactory.getCurrentSession().save(user);
    }

    /*
     * Изменение статуса пользователя, после его прохождения по ссылке
     */
    public void changeChecking(String login) {
        Users user = getUser(login);
        user.setEnabled(true);
        sessionFactory.getCurrentSession().update(user);
    }

    /*
     * Изменение имени пользователя
     */
    public void setName(String name, String login) {
        Users user = getUser(login);
        user.setName(name);
        sessionFactory.getCurrentSession().update(user);
    }

    /*
     * Изменение логина пользователя
     */
    public void setLogin(String login, String oldLogin) {
        Users user = getUser(oldLogin);
        user.setLogin(login);
        sessionFactory.getCurrentSession().update(user);
    }

    /*
     * Изменение пароля пользователя
     */
    public void setPassword(String hash_pass, String login) {
        Users user = getUser(login);
        user.setHash_pass(hash_pass);
        sessionFactory.getCurrentSession().update(user);
    }
}
